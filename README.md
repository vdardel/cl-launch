cl-launch is a unix utility to make your Lisp software easily invokable from the shell command-line.

cl-launch will help you invoke your Lisp software from the Unix command-line or as a `#!` script. We encourage you to install cl-launch 4.1.2 or 
later and write scripts using `#!/usr/bin/cl`. A trivial shim written in C is required for it to work on BSD kernels. 

It is a self-contained shell-script by Fare Rideau that will abstract away the details of the underlying Lisp implementation
by generating the proper Lisp and shell code. Extensive automated testing ensures that it works in thousands of valid combined operation modes, Lisp and shell implementations.

Examples:

```
#!/usr/bin/cl -sp lisp-stripper -E main
(defun main (argv)
  (if argv
      (map () 'print-loc-count argv)
      (print-loc-count *standard-input*)))
```
      
Read more on Cliki: http://www.cliki.net/cl-launch

## Install

cl-launch is in Debian:

    apt-get install cl-launch


## Licence

MIT.